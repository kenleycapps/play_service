action :disable do
	supervisor_service @new_resource.name do
		action :disable
	end
end

action :enable do
	verify_play_app(@new_resource.path)
	bin_path = ::File.join(@new_resource.path, "bin")
	run_file_path = ::File.join(bin_path, get_run_file(bin_path))

	run_opts_mem = @new_resource.run_opts_mem
	run_opts_evolutions = @new_resource.run_opts_evolutions
	run_opts_args = @new_resource.run_opts_args

	supervisor_service @new_resource.name do
		action :enable
		autostart true
		autorestart true
		directory bin_path
		command "#{run_file_path} -mem #{run_opts_mem} -DapplyEvolutions.default=#{run_opts_evolutions} #{run_opts_args}"
		user user
	end
end

def verify_play_app(path)
	bin_path = ::File.join(path, "bin")
	run_file = get_run_file(bin_path)
	Chef::Application.fatal!("Play start script not found under #{bin_path}", 1) if run_file.nil?

	run_file_path = ::File.join(bin_path, run_file)
end


def get_run_file(bin_path)
	# For a play web app packaged with 'activator dist'
	# We are after APP_PATH/bin/runfile but we don't know the name of 'runfile'
	# 
	# Example:
	# APP_PATH/
	# 	./bin/
	#       ./runfile
	#       ./runfile.bat
	# 
	# TERRIBLE HACK INCOMING:
	# Fortunately there's a runfile.bat, so we can search for *.bat and extract

	extension = ".bat"
	win_run_file = ::Dir[::File.join(bin_path, "*#{extension}")].first
	return ::File.basename(win_run_file, extension) unless win_run_file.nil?
end