action :setup do
	# Setup params
	root = ::File.join(@new_resource.path, "current")
	zipfile = "sample-app-1.0-SNAPSHOT.zip"
	remote_zipfile = "https://s3.amazonaws.com/chef_remote_files/play_service/#{zipfile}"
	zipfile_full_path = ::File.join(root, zipfile)
	result_dir = ::File.join(root, ::File.basename(zipfile_full_path, ".zip"))

	Chef::Log.info("Setting up sample app at #{root}")

	# Setup directories/files
	directory root do
		recursive true
		action :create
	end

	remote_file zipfile_full_path do
		source remote_zipfile
		action :create_if_missing
	end

	# Unpack
	bash "unpack_sample_app" do
		code "unzip #{zipfile_full_path} -d #{root} && mv #{root}/**/* #{root}"
		not_if { ::File.exists?(result_dir) }
	end
end