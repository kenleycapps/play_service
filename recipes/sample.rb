# Install dependencies
package 'unzip'

# Sets up a single sample play app at the configured path
play_service_sample node['play_service']['sample']['path']