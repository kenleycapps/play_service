include_recipe 'java'
include_recipe 'python'

# Ensure setuptools is upgraded, as supervisor dependency makes use of newer version calls (e.g. egg_info)
python_pip "setuptools" do
  action :upgrade
end

include_recipe 'supervisor'