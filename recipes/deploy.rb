opsworks_disabled = node[:play_service][:disable_opsworks]

include_recipe 'deploy' unless opsworks_disabled
node[:deploy].each do |application, deploy|
	unless opsworks_disabled
		opsworks_deploy_dir do
		    user deploy[:user]
		    group deploy[:group]
		    path deploy[:deploy_to]
		  end

		  opsworks_deploy do
		    deploy_data deploy
		    app application
		  end
	end

	play_service deploy[:application] do
		path ::File.join(deploy[:deploy_to], "current")
		user deploy[:user]

		run_opts_mem node[:play_service][:run_opts][:mem]
		run_opts_evolutions node[:play_service][:run_opts][:evolutions]
		
		run_opts_args node[:play_service][:run_opts][:args]
	end
end