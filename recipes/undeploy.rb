node[:deploy].each do |application, deploy|
	play_service deploy[:application] do
		action :disable
		path deploy[:deploy_to]
		user deploy[:user]
	end
end