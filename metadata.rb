name "play_service"
version "0.0.1"

depends "supervisor"
depends "java", "~> 1.24.0"
depends "python"