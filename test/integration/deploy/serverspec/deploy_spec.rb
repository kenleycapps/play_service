require 'spec_helper'

describe "deployed play service" do
	it "is listening on port 9000" do
		expect(port(9000)).to be_listening
	end

	it "is running supervisor" do
		expect(service("supervisor")).to be_running
	end
end