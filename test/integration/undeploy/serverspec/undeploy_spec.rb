require 'spec_helper'

describe "undeployed play service" do
	it "is not listening on port 9000" do
		expect(port(9000)).not_to be_listening
	end
end