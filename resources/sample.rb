# Represents a sample play app
actions :setup
default_action :setup

attribute :path, :kind_of => String, :name_attribute => true, :required => true