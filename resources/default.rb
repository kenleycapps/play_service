# Represents a play service
actions :enable, :disable
default_action :enable

attribute :name, :kind_of => String, :name_attribute => true, :required => true
attribute :path, :kind_of => String, :required => true
attribute :user, :kind_of => String, :required => true

# Run options
attribute :run_opts_mem, :kind_of => Fixnum, :required => false, :default => 512
attribute :run_opts_evolutions, :kind_of => [TrueClass, FalseClass], :required => false, :default => false

# General run options
attribute :run_opts_args, :kind_of => String, :required => false