default[:play_service][:run_opts][:mem] = 512
default[:play_service][:run_opts][:evolutions] = true
default[:play_service][:run_opts][:args] = ''
default[:play_service][:disable_opsworks] = false

default[:java][:jdk_version] = "7"
